import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class marugameGUI {
    private JTextPane receivedInfo;
    private JButton kitsune;
    private JButton bukkake;
    private JButton kamatama;
    private JButton kamaage;
    private JButton zaru;
    private JButton mentai;
    private JButton torotama;
    private JButton kake;
    private JLabel category1;
    private JLabel category2;
    private JLabel category3;
    private JLabel category4;
    private JLabel order;
    private JButton karaage;
    private JButton onioroshi;
    private JLabel category5;
    private JTextPane totalInfo;
    private JLabel total;
    private JPanel root;
    private JButton billButton;

    int totalPlice=0;
    int warmChill=0;
    String size;
    String temperture;


    public void order(String food, int price){
        int confirming1 = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " for " + price + " yen?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirming1 == 0) {
            String selectSize[] = {"regular", "large(+160yen)", "extra-large(+320yen)"};
            int confirming2 = JOptionPane.showOptionDialog(null,
                    "What size should you choose？",
                    "Size Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    selectSize,
                    selectSize[0]
            );
            if (confirming2 == 0) {
                totalPlice+=price;
                size = "regular";
            }else if(confirming2==1){
                totalPlice+=price+160;
                size = "large";
            }else {
                totalPlice += price + 320;
                size = "extra-large";
            }

            if(warmChill==1){
                String selectwarmChill[] = {"warmed", "chilled"};
                int confirming3 = JOptionPane.showOptionDialog(null,
                        "There are warmed and chilled ones？",
                        "Warmed and Chilled Confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        selectwarmChill,
                        selectwarmChill[0]
                );
                if (confirming3 == 0) {
                    temperture="warmed";
                }else {
                    temperture="cilled";
                }
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + "(" + size +")(" + temperture + ")\n");
            }else{
                String currentText = receivedInfo.getText();
                receivedInfo.setText(currentText + food + "(" + size +")\n");
            }
            totalInfo.setText(String.valueOf(totalPlice));
        }
    }

    public marugameGUI() {
        onioroshi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("onioroshi", 840);
            }
        });

        karaage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("karaage", 820);
            }
        });

        kake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=1;
                order("kake", 390);
            }
        });

        kitsune.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=1;
                order("kitsune", 560);
            }
        });

        bukkake.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=1;
                order("bukkake", 380);
            }
        });

        torotama.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=1;
                order("torotama", 580);
            }
        });

        kamatama.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("kamatama", 500);
            }
        });

        mentai.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("mentai", 590);
            }
        });

        kamaage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("kamaage", 340);
            }
        });

        zaru.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                warmChill=0;
                order("zaru", 370);
            }
        });

        billButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation4=JOptionPane.showConfirmDialog(null,
                        "Would you like to complete your order?",
                        "Finish Order",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation4 == 0){
                    JOptionPane.showMessageDialog(null,
                            "The bill is " + totalPlice + "yen.");
                    totalPlice=0;
                    receivedInfo.setText("");
                    totalInfo.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("marugameGUI");
        frame.setContentPane(new marugameGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
